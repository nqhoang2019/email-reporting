import random
import csv, os, glob
import string, json
import re
import datetime

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_uppercase 
    return ''.join(random.choice(letters) for i in range(stringLength))

    
def randomDigit(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.digits 
    return ''.join(random.choice(letters) for i in range(stringLength))

def removeDoubleSpace(str):
    str = re.sub(' +', ' ', str)
    str = re.sub('^ +', '', str)
    str = re.sub(r' +\.', '.', str)
    return str

def read_json(path):
    with open(path) as f:
        return json.load(f)

def removeSpecialCharacters(str):
    chars = [
        "'",' ',',', '+', '#'
    ]
    for c in chars:
        str = str.replace(c, "_")
    return re.sub('_+', '_', str)


def read_txt(path, skip_header=False, get_string=False):
    with open(path) as f:
        rows = f.read().splitlines()
    if not get_string:
        return rows[1:] if skip_header else rows
    else:
        return '\n'.join(rows[1:] if skip_header else rows)

def read_csv(path, skip_header=True):
    rows = []
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file)
        for row in csv_reader:
            rows.append(row)
    return rows[1:] if skip_header else rows

def read_tdt(path, skip_header_rows=0):
    rows = []    
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='\t')
        for row in csv_reader:
            rows.append(row)
        return rows[skip_header_rows:]

def save_tdt(data, path):
    with open(path, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter='\t')
        writer.writerows(data)

def remove_special_chars(str):
    return ''.join(e for e in str if e.isalnum())

def save_csv(data, path):
    with open(path, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(data)
    f.close()

def save_txt(data, path):
    with open(path, 'w') as f:
        f.writelines([f"{row}\n" for row in data])
    f.close()

def get_filename_from_path(path):
    return path.split("\\")[-1] if "\\" in path else path.split('/')[-1]

def get_direct_ggdrive(link):
    direct_link = "https://drive.google.com/uc?export=download&id=ID_HERE"
    id = link.split('/')[-2].split('?')[0]
    return direct_link.replace('ID_HERE', id)

def combine_tdt_files(header_file, files_path, result_file):
    files = glob.glob(f'{files_path}*.txt')

    data = read_tdt(header_file)
    for f in files:
        data.extend(read_tdt(f, 3))
    save_tdt(data, f"{result_file}")    

def date_jumpback(days=0):
    today = datetime.date.today()
    jump = today - datetime.timedelta(days = days)
    return jump
