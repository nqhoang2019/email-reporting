import utils, os
from email_utils import send_email
from datetime import datetime


CONTENT_FILE = os.environ['PROCESS_LOG']
SUPPORT_EMAIL = os.environ['SUPPORT_EMAIL'] or "nqhoang11ctt@gmail.com"
SERVER_EMAIL = os.environ['SERVER_EMAIL'] or "nqhoang11ctt@gmail.com"
REMOVE_LOG = True
# print(CONTENT_FILE)

try:
    data = utils.read_txt(CONTENT_FILE, get_string=True)
    # print(data)
    report_date = datetime.today().strftime('%Y-%m-%d')
    body = f"""
    Order processing log for {report_date} from {SERVER_EMAIL}
    ======================================

    {data}
    """


    if send_email(SERVER_EMAIL, SUPPORT_EMAIL, f"[ZILODA] Order Process Report - {report_date}", body):
        print("Sent!")

    if REMOVE_LOG:
        os.remove(CONTENT_FILE)
except Exception as err:
    print(err)